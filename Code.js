// List of "Job Folder-Folders"
var jobFolderIds = {
  Pending:'17kfUZ2b8qiHFRKbEq7denAvrhms-lNfP',
  Active:'0B6xAEhRdM9A_RjhwSnM1QU5WbDA',
  Closed:'1zJrWc4UDg2Tz84akvP9oIndJ3km4X5uW',
  Complete:'0B6xAEhRdM9A_OGg1TkdUdEdhclE',
  Held:'1jZNByCg17MEGGs4e5v7mknuTdVu9VAGJ'
};

// Returns a webpage on webapp load from the index template.
function doGet(request) {
  return HtmlService.createTemplateFromFile('Index')
      .evaluate();
}

// Allows the webpage to call additional objects
function include(filename) {
  return HtmlService.createHtmlOutputFromFile(filename)
      .getContent();
}

// TODO: Make this accept comma seperate lists for non-numeric job folders
/**
 * Orchestrates the finding of one or more sequentially numbered job folders.
 * @param {Number} start Start of range
 * @param {Number}  end End of Range
 * @return {object} Returns a 2 dimensional array of found and lost folders
 */
function finder_iterator_range(start,end){
  var foundFolders = [];
  var lostFolders = [];
  Logger.log("Iterating over " + (end-start) +" chambers"); 
  for(i=start; i <= end; i++){
    searchResults = folder_finder(i)[0];
    Logger.log(searchResults)
    if(searchResults == -1) {
      Logger.log("More than one folder containing job number \""+i+"\" found! \n Skipping job number.")
      lostFolders.push("Folder \""+i+"\" found more than once! Please verify only one job folder exists per job!");
    }
    if(searchResults == -2) {
      Logger.log("Folder containing job number \""+i+"\" could not be found! \n Skipping job number.")
      lostFolders.push("Folder \""+i+"\" not found! Please verify job folder exists!");
    }
    else foundFolders.push(searchResults);
  }
  Logger.log(foundFolders)
  return [foundFolders,lostFolders]
 
}


/**
 * Finds folders inside the active jobs folder. Can/Should be adapted to a common function.
 *
 * @param {string} folder_number Folder to search for
 * @param {boolean} listDuplicates Wether or not to list duplicate results as an array, rather than returning error code -1 for folders with duplicates
 * @return {object} An object containing an error code or one of: folder objects if list-duplicates is set to false, and one or more folder object if list_duplicates is true
 */
function folder_finder(folder_number, listDuplicates) {
  if (listDuplicates == null) {
    listDuplicates = false;
  }
  var resultList = []; 
  for (const status in jobFolderIds){
    var folders = DriveApp.getFolderById(jobFolderIds[status]).searchFolders("title contains '"+folder_number+"_'");
    try{
      hasNext = folders.hasNext();
    }
    catch(err){
      hasNext = false;
      }
    Logger.log('User\'s drive contains folder containing \''+folder_number+"\':     "+hasNext);
    
    if(hasNext == true){
      while(folders.hasNext()){
        resultList.push(folders.next());
      }
    }
  }
  if (listDuplicates === false){
    if(resultList.length > 1){
      return([-1]);
    }
    if(resultList.length == 0){
      return([-2]); 
    }
    else{
      return (resultList);
    }
  }
  else{
    return resultList;
  }
}


/**
 * Finds all folders in a source folder
 *
 * @param {string} Folder to search f or
 * @param {boolean} Wether or not to list duplicate results as an array, rather than returning error code -1 for folders with duplicates
 * @return {object} An object containing an error code or one of: folder objects if list-duplicates is set to false, and one or more folder object if list_duplicates is true
 */
function folder_in_folder_finder(folderID) {
  var resultList = []; 
  var folders = DriveApp.getFolderById(folderID).getFolders();  
  while(folders.hasNext()){
      resultList.push(folders.next());
  }
  Logger.log(resultList)
  return resultList;
}

/**
 * Formats a list of files for display within a google webapp.
 * @param {object} folderArray List of folder objects
 * @return {object} Array containing folders as a list of their values instead of as folder objects
 */
function folder_display(folderArray){
  var pageContent = [];
  for (var i = 0; i < folderArray.length; i++){
    pageContent.push([folderArray[i].getName(), folderArray[i].getUrl(), folderArray[i].getId()]);
  }
  Logger.log(pageContent);
  return pageContent;
}

/**
 * Searches for a specific file within a folder
 * @param {string} searchID Folder ID to search within
 * @param {string} filername String to search for within targeted children
 * @return {object} one or more file objects
 */
function file_in_folder_finder(folderID,filename) {
  var resultList = []; 
  var folders = DriveApp.getFolderById(folderID).searchFiles("title contains '"+filename+"'");  
  while(folders.hasNext()){
      resultList.push(folders.next());
  }
  Logger.log(resultList)
  return resultList;
}

/**
 * Searches for a specific folder within another folder
 * @param {string} searchID Folder ID to search within
 * @param {string} foldername String to search for within targeted subfolders
 * @param {boolean} listDuplicates Wether or not to return folders with multiple matches
 * @return {object} An object containing an error code or one of: folder objects if list-duplicates is set to false, and one or more folder object if list_duplicates is true
 */
function search_folder_in_folder(searchID,foldername, listDuplicates) {
    if (listDuplicates == null) {
    listDuplicates = false;
  }
  var resultList = []; 
  var folders = DriveApp.getFolderById(searchID).searchFolders("title contains '"+foldername+"'"); 
  if (!listDuplicates){
  while(folders.hasNext()){
    folder = folders.next()
    if (folder.getName() == foldername) return folder
  }
  }else{
    while(folders.hasNext()){
      resultList.push(folders.next())
    }
    return resultList
  }
}