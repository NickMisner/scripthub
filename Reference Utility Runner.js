// TODO: Accept Comma Sepperated Lists
/**
 * Creates the needed "source folders" list to be added to other job folders, based off a single folder.
 * @param {number} start Starting job folder number in range
 * @param {number} end Final Job folder numberin range- Folders with numbers between this and start will be linked to the Source folder.
 * @param {string} source String representing the job number to reference.
 * @return {string} A string indicating success or failure, as well as any accompanying error codes.
 */
function referenceRunner(start,end,source) {
  try{
    var sourceFolder = folder_finder(source)[0]
    if (sourceFolder == -1) return ("Source Folder \""+source+"\" found more than once! Please verify only one job folder exists per job!");
    var sourceFolders = ReferenceFolderUtility.reference_folder_creator(sourceFolder);
    var finderResults = finder_iterator_range(start,end);
    var foundFolders = finderResults[0];
    var lostFolders = finderResults[1];
    var referenceText = []
    // TODO: Make this make more sense- dipping in and out of the Reference Folder utility is a nightmare for following this code.
    ReferenceFolderUtility.folder_handler(foundFolders, sourceFolders,source);
    if (lostFolders.length > 0){
      returnText = "The utility completed the placing reference folders in the following Job Folders without error: <br><br>"
      foundFolders.forEach(function(text){returnText += text;})
      returnText += "<br> There where issues finding the following Job Folders: <br><br>" 
      lostFolders.forEach(function(text){returnText += text;})
      returnText += "<br> If a folder was not found, it may not exist, may be numbered incorrectly, or may have duplicates. <br>"
      returnText += "After checking for the listed issues, this utility can be run again against folders that where not found."
    }
    else{
      returnText = "Success, with no exceptions!"
      }
    return returnText
  }
  catch(err){
    return ("Failed with error: <br>"+err)
  }
}
