function folder_display_temp(folderNumber){
 //nest this in code.gs as part of the main project code. 
 //It will likely get re-used by the edit folders function
  
  /*Sudo code:
  Folders = Run folder_finder with list_duplicates attribute set to true
  for each folder in folders
     output to webpage the following:
        [[folder name, folder link, folder_id],...]
        
  ****End Of Display portion****
  */
  var folders = folder_finder(folderNumber, true);
  var pageContent = [];
  for (var i = 0; i < folders.length; i++){
    pageContent.push([folders[i].getName(), folders[i].getUrl(), folders[i].getId()]);
  }
  Logger.log(pageContent);
  return pageContent;
}


/*in webpage, create logic to display rows with link to folder and checkbox to mark for
* deletion. When checkbox is clicked and "delete" is pressed, prompt for confirmation
* and launch the next script. If no checkbox is selected, present error prompt
*/

function delete_folders(folderArray){
//[name,id] strings
  var success = [];
  for(var i = 0; i < folderArray.length; i++){
        DriveApp.getFolderById(folderArray[i][1]).setTrashed(true);
        success.push(folderArray[i][0]);
        }
  return(success);
}