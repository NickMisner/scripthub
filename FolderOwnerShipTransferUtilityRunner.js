// Output is extracted to a global here to allow the iterative nature of this function to complete
var output = "Output: \n";

function run_Folder_Ownership_Transfer(targetOwner,targetFolder){

  

  if(typeof(targetFolder) == "string"){
  targetFolder = DriveApp.getFolderById(target);
  }

  var targetOwnerCheck = /(.*)@associatedenvironmentalsystems.com/
  if (targetOwnerCheck.test(targetOwner)) return('Failed because target owner was not a valid @associatedenvironmentalsystems.com email');
  
  //Change ownership of files in current folder
  var targetFiles = targetFolder.getFiles();
  while (targetFiles.hasNext()){
   var file = sourceFiles.next();
   try{
   file.setOwner(targetOwner);
   }catch(e){
     output = output + 'Could not set owner of file '+file.getName()+'.\n';
   }
  }
  
  
 //Descend Recursively into folder structure
  var targetFolders = targetFolder.getFolders();
  var targetFolderIterator = {};
  while (sourceFolders.hasNext()){
   var folder = sourceFolders.next();
   try{
     folder.setOwner(targetOwner);
     run_Folder_Ownership_Transfer(targetOwner, folder);
   }catch(e){
     output = output + 'Could not set owner of folder '+folder.getName()+'.\n';
   }
}
  return output
}