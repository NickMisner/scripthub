function addViewersAdvanced_(viewersemails, id){
    for (var i = 0; i < viewersemails.length; i++) {
     var retries = 20;
      while(retries>0){
        try {
          Drive.Permissions.insert(
            {
              'role': 'reader',
              'type': 'user',
              'value': viewersemails[i]
            },
            id,
            {
              'sendNotificationEmails': 'false'
            });
          break;
        } catch(except){
          Utilities.sleep(100);
          retries --;
        }
      }
  }
}

function addEditorAdvanced_(editorsemails, id){
  Logger.log(editorsemails);
  for (var i = 0; i < editorsemails.length; i++) {
    var retries = 20;
      while(retries>0){
        try {
    Logger.log(editorsemails[i]);
    Drive.Permissions.insert(
      {
        'role': 'writer',
        'type': 'anyone',
        'value': editorsemails[i]
        
      },
      id,
      {
        'sendNotificationEmails': 'false'
      });
           break;
        } catch(except){
          Logger.log(except);
          retries --;
        }
      }
  }
}

function deletePermissions_(target){  
  var list = Drive.Permissions.list(target.getId());
    
  for (var i=0; i<list.items.length; i++){
    if (list.items[i].emailAddress != "no-reply@associatedenvironmentalsystems.com"){
      Drive.Permissions.remove(target.getId(), list.items[i].id);
    }
  }
}

function copyPermissions(source, target){
  var editors = source.getEditors();
  var viewers = source.getViewers();
  var editorEmails = [];
  var viewerEmails = [];
  for (var i=0; i<editors.length; i++){
    if(editors[i].getEmail().length>0)
      editorEmails.push(editors[i].getEmail());
  }
  for (var i=0; i<viewers.length; i++){
    if(viewers[i].getEmail().length>0)
      viewerEmails.push(viewers[i].getEmail());
  }
  addEditorAdvanced_(editorEmails, target.getId());
  addViewersAdvanced_(viewerEmails, target.getId());
}
 
function copyFile(source, target) {
  var copy = source.makeCopy(file.getName(), target);
  copyPermissions(source, copy);
}